# Realworld [django](https://github.com/gothinkster/django-realworld-example-app) + [vue](https://github.com/gothinkster/vue-realworld-example-app)

This folder contains two detached and independent servers implementing a [Medium.com](medium.com) clone. A [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer)
backend written in [Django](https://www.djangoproject.com/) and a [Vue.js](https://vuejs.org/)
frontend.

Complete credit to the [realworld project](https://github.com/gothinkster/realworld)
for such a valuable contribution, go check them out.

Check the *backend* and the *frontend* directories for an overview.

Both servers run without system-wide changes (other than basic dependencies).
The servers are preconfigured to run in `localhost` but they can be configured
to run in production environments.

The installation instructions assume you are in the root folder of the repo:
```
	$ git clone https://gitlab.com/da_doomer/realworld-vue-django.git
	$ cd realworld-vue-django
```

# Backend

The backend provides a RESTful API to provide blog capabilities such as writing
posts, following users, having a personal feed, etc.

Handles sessions and serialization. When a request is made to the server the
server responds with the appropiate information (such as the user's feed) or
carries out the requested action (such as publishing a comment).

Uses sqlite, which only creates a file on the local folder to provide SQL
database capabilities.

## Installation
1. Install python3
2. Install sqlite
3. Set-up the virtual environment:
```
	$ cd backend-django
	$ python -m pip install pipenv
	$ python -m pipenv install
	$ python -m pipenv shell
	$ python manage.py migrate
```
4. Run the server:
```
	$ python manage.py runserver
```

# Frontend

The frontend responds with HTML to requests by users. When a user attempts to
visit the website a request is made to the frontend server which in turn
answers with a website filled with information provided by the backend server.

## Installation
1. Install nodejs
2. Install yarn
3. Set-up the virtual environment:
```
	$ cd frontend-vue
	$ yarn install
```
4. Run the server:
```
	$ yarn serve
```

If both servers are running you can visit the website on
[http://localhost:8080](http://localhost:8080)

# Additional information

Profile modification is not supported at the moment.
